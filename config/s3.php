<?php
return [
 's3_access_key' => env('AWS_ACCESS_KEY_ID'),
 's3_secret_key' => env('AWS_SECRET_ACCESS_KEY'),
 's3_region' => env('AWS_DEFAULT_REGION'),
 's3_bucket' => env('AWS_BUCKET'),
 's3_host' => env('AWS_URL'),
];