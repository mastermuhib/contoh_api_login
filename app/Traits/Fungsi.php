<?php 

namespace App\Traits;

use Illuminate\Http\Request;
use App\MenuModel;
use DB;

trait Fungsi {
    
    public static function getmenu($role_id)
    {
        $menu = DB::table('elearning_menus')->join('elearning_role_menus','elearning_menus.id','=','elearning_role_menus.menu_id')->where('role_id',$role_id)->where('elearning_menus.parent_menu_id',0)->select('elearning_menus.menu_name as name','serial_number','slug','icon','elearning_menus.menu_id as menu_id','parent_menu_id')->orderBy('serial_number','asc')->get();
        
        return $menu;   

    }

    public static function getmenuall()
    {
        $menu = DB::table('elearning_menus')->where('elearning_menus.parent_menu_id',0)->select('elearning_menus.menu_name as name','serial_number','slug','icon','elearning_menus.menu_id as menu_id','parent_menu_id','elearning_menus.id as id')->orderBy('serial_number','asc')->get(); 
         // print_r($menu);
         // exit();
        return $menu;   

    }

    public static function getsubmenu($role_id)
    {
        $submenu = DB::table('elearning_menus')->join('elearning_role_menus','elearning_menus.id','=','elearning_role_menus.menu_id')->where('role_id',$role_id)->where('elearning_menus.menu_id',0)->select('elearning_menus.menu_name as name','serial_number','slug','icon','elearning_menus.menu_id as menu_id','parent_menu_id')->orderBy('serial_number','asc')->get();
        return $submenu;
    }

    public static function getsubmenuall()
    {
        $submenu = DB::table('elearning_menus')->where('elearning_menus.menu_id',0)->select('elearning_menus.menu_name as name','serial_number','slug','icon','elearning_menus.menu_id as menu_id','parent_menu_id','elearning_menus.id as id')->orderBy('serial_number','asc')->get();
        return $submenu;
    }
    public static function kodeToken($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 16; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function getdataUser($type,$id) {
        $data = DB::table('field_datas')->where('id_user',$id)->where('field_type_id',$type)->pluck('data');
        if (!empty($data[0])) {
            $dataku =$data[0];
        } else {
            $dataku ='-';
        }
        return $dataku;
    }
    // jumlah peserta kelas
    public static function getjumlahPeserta($kelas_id)
    {
        $jumlah = DB::table('elearning_kelas_user')->where('id_kelas',$kelas_id)->get();
        $return = count($jumlah);
        return $return;   

    }

    // jumlah modul per subjek
    public static function getjumlahModul($id_subjek)
    {
        $jumlah = DB::table('elearning_subjek_modul')->where('id_subjek',$id_subjek)->get();
        $return = count($jumlah);
        return $return;   

    }
    // cek member
    public static function getcekmember($id) {
        $data = DB::table('user_registrations')->where('id_user',$id)->pluck('is_member');
        if (!empty($data[0])) {
            $dataku =$data[0];
        } else {
            $dataku ='-';
        }
        return $dataku;
    }
    public static function namaCategory($id) {
        $data = DB::table('elearning_category_content')->where('id',$id)->pluck('category_content_name');
        if ($data->isNotEmpty()) {
            $dataku =$data[0];
        } else {
            $dataku ='-';
        }
        return $dataku;
    }
    public static function descCategory($id) {
        $data = DB::table('elearning_category_content')->where('id',$id)->pluck('description');
        if ($data->isNotEmpty()) {
            $dataku =$data[0];
        } else {
            $dataku ='-';
        }
        return $dataku;
    }
    // jumlah row

}

?>