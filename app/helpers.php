<?php

function HariIndo($hari1){
    $day = new DateTime($hari1);
    $hari = $day->format('l');
    if($hari == 'Monday') {
       $return = "Senin";
    } else if($hari == 'Tuesday') {
       $return = "Selasa";
    } else if($hari == 'Wednesday') {
       $return = "Rabu";
    } else if($hari == 'Thursday') {
        $return = "kamis";
    } else if($hari == 'Friday') {
        $return = "Jumat";
    } else if($hari == 'Saturday') {
        $return = "Sabtu";
    } else if($hari == 'Sunday') {
        $return = "Minggu";
    }
    return $return;
}
function CekAbsen($modul,$user){
    $cek = DB::table('elearning_absensi')->where('modul_id',$modul)->where('user_id',$user)->where('is_finish',2)->get();
    if ($cek->isEmpty()) {
        $return = 0;
    } else {
        $return = 1;
    }

    return $return;
}
function CekExam($exam,$subjek,$user){
    $cek_selesai = DB::table('elearning_absensi')->join('elearning_subjek_modul','elearning_subjek_modul.id','=','elearning_absensi.modul_id')->where('user_id',$user)->where('id_subjek',$subjek)->where('is_finish',1)->get();
    if ($cek_selesai->isEmpty()) {
        $cek = DB::table('elearning_absensi')->where('id_exam',$exam)->where('user_id',$user)->where('is_finish',2)->get();
        if ($cek->isEmpty()) {
            $return = 0;
        } else {
            $return = 1;
        }
    } else {
       $return = 2;
    }

    return $return;
}
function CekQuiz($quiz,$user){
    $cek = DB::table('elearning_absensi')->where('id_quiz',$quiz)->where('user_id',$user)->where('is_finish',2)->get();
    if ($cek->isEmpty()) {
        $return = 0;
    } else {
        $return = 1;
    }

    return $return;
}

function C_quess($id){
    $data = DB::table('elearning_answer_options')->where('question_id',$id)->get();
    $count = count($data);
    return $count;
}

function CekJawaban($user,$exam,$soal,$jawaban){
    $data = DB::table('elearning_user_answer')->where('id_user',$user)->where('id_exam',$exam)->where('id_question',$soal)->where('id_answer',$jawaban)->get();
    $count = count($data);
    return $count;
}
function CekJawabanQuiz($user,$quiz,$soal,$jawaban){
    $data = DB::table('elearning_quiz_answer')->where('id_user',$user)->where('id_quiz',$quiz)->where('id_question',$soal)->where('id_answer',$jawaban)->get();
    $count = count($data);
    return $count;
}
function CekSoal($user,$exam,$soal){
    $data = DB::table('elearning_user_answer')->where('id_user',$user)->where('id_exam',$exam)->where('id_question',$soal)->pluck('id_answer');
    if ($data->isEmpty()) {
        $return = "-";
    } else {
        $return = $data[0];
    }
    
    return $return;
}
function CekSoalQuiz($user,$quiz,$soal){
    $data = DB::table('elearning_quiz_answer')->where('id_user',$user)->where('id_quiz',$quiz)->where('id_question',$soal)->pluck('id_answer');
    if ($data->isEmpty()) {
        $return = "-";
    } else {
        $return = $data[0];
    }
    
    return $return;
}
function CekKunci($soal){
    $data = DB::table('elearning_answer_options')->where('question_id',$soal)->where('is_correct',1)->pluck('id');
    if ($data->isEmpty()) {
        $return = "-";
    } else {
        $return = $data[0];
    }
    
    return $return;
}
function cek_modul_aktif($user,$modul)
{
    $modul = DB::table('elearning_absensi')->where('user_id',$user)->where('modul_id',$modul)->pluck('is_finish');
    if ($modul->isEmpty()) {
        $return = 0;
    } else {
        $return = $modul[0];
    }
    
    return $return;   

}
function cek_exam_aktif($user,$exam)
{
    $modul = DB::table('elearning_absensi')->where('user_id',$user)->where('id_exam',$exam)->pluck('is_finish');
    if ($modul->isEmpty()) {
        $return = 0;
    } else {
        $return = $modul[0];
    }
    
    return $return;   

}
function cek_quiz_aktif($user,$quiz)
{
    $modul = DB::table('elearning_absensi')->where('user_id',$user)->where('id_quiz',$quiz)->pluck('is_finish');
    if ($modul->isEmpty()) {
        $return = 0;
    } else {
        $return = $modul[0];
    }
    
    return $return;   

}
function HariAbsen($jenis,$user,$id)
{
    if ($jenis == "modul") {
        $modul = DB::table('elearning_absensi')->where('user_id',$user)->where('modul_id',$id)->pluck('date');
        $return = $modul[0];
    } elseif($jenis == 'exam'){
        $modul = DB::table('elearning_absensi')->where('user_id',$user)->where('id_exam',$id)->pluck('date');
        $return = $modul[0];
    } else {
        $modul = DB::table('elearning_absensi')->where('user_id',$user)->where('id_quiz',$id)->pluck('date');
        $return = $modul[0];
    }  
    
    return $return;   

}
?>