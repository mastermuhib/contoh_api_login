<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $table = 'sales';
    protected $guarded = [''];
    protected $hidden   = ['created_at','updated_at'];
    protected $date = ['deleted_at'];
    public $incrementing = false;
}
