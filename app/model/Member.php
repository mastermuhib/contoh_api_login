<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table 	= 'users';
    protected $fillable = ['id','id_user','status','deleted_it', 'created_at','updated_at','token'];

    protected $casts = [
        'id' => 'string',
    ];
    protected $primaryKey = 'id';
    public $incrementing = false;
}
