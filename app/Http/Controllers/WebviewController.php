<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Article;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Auth;
use DB;
use App\Classes\S3;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class WebviewController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function get_home()
    {
        //dd(date('Y-m-d',strtotime('-1 day')));
        try {
            //$session_id = 'cjVGU0NVT3EyN1NyZUs0QVd4Y21ZQjJmS2xsYkxrcUp3QkNrOUdQMg==';
            $session = Auth::user()->token;
            $cek_session = DB::table('users')->where('token',$session)->get();   
            $data['token'] = base64_encode($session);
            $data['is_mobile'] = 'true';
            $data['name'] = $cek_session[0]->name;
            $data['id'] = base64_encode($session);
            return view('welcome',$data);
        } catch (Exception $e) {
            $data['status'] = 500;
            $data['message'] = 'Caught exception: '. $e->getMessage() ."\n";
            return response()->json($data);
        }
    }
    // detail berita
    public function test()
    {
        echo "test";
    }
    //All informasi
}