<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
| kita coba
|

*/
$router->get('/test', 'WebviewController@test');
$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('/get_home', 'WebviewController@get_home');
    // home
    $router->post('/profile', 'AkunController@postProfile');
    $router->post('/register', 'RegisterController@register');
    $router->get('/mulai/{id}/{token}', 'ModulController@index');
    // informasi
});