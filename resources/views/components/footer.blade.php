<!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light text-center navbar-shadow">
    	<div class="col-md-12">
        <p class="clearfix blue-grey lighten-2 mb-0"><span class="d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2020<a class="text-bold-800 grey darken-2" href="https://www.cmskandidat.co.id" target="_blank">Meta,</a>All rights Reserved</span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button>
        </p>
    </footer>
<!-- END: Footer-->