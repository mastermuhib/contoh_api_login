<!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="javascript:void(0)">
                        <div class="brand-logo"></div>
                        <h2 class="brand-text mb-0">E-Learning</h2>
                    </a></li>
                <!-- <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i></a></li> -->
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">

            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                @foreach($menu as $k => $v)
                    @if ($v->slug == 'dashboard' || $v->slug == 'list-user' || $v->slug == 'kelas' || $v->slug == 'absensi')
                        <li class=" nav-item"><a href="/{{$v->slug}}"><i class="{{$v->icon}}"></i><span class="menu-title" data-i18n="{{$v->name}}">{{$v->name}}</span></a></li>
                    @else
                        <li class=" nav-item"><a href="javascript:void(0)"><i class="{{$v->icon}}"></i><span class="menu-title" data-i18n="{{$v->name}}">{{$v->name}}</span></a>
                            <ul class="menu-content">
                            @foreach($submenu as $a => $s)  
                            <?php if ($v->menu_id == $s->parent_menu_id) { ?>
                                <li><a href="/{{$s->slug}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Shop">{{$s->name}}</span></a>
                                
                            <?php } ?>
                            @endforeach
                            </ul>
                        </li>
                    @endif
                @endforeach
                
            </ul>
        </div>
    </div>
<!-- END: Main Menu-->