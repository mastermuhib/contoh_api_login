<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="E-Learning Meta Atmi Didactic"><meta name="keywords" content="Meta Atmi Didactic">
    <title>E-learning</title>
    <link rel="apple-touch-icon" href="{{URL::asset('assets')}}/images/ico/apple-icon-120.png">
    
    <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset('assets')}}/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/extensions/nouislider.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/ui/prism.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/vendors/css/forms/select/select2.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/css/colors.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/css/components.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/css/plugins/extensions/noui-slider.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets')}}/css/pages/app-ecommerce-shop.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="../../../assets/css/style.css">
    <style type="text/css">
        .wrapper-1 {
          background: white;
          max-width: 400px;
          margin-top: 5px auto;
          padding-left: 10px;
        }
        .demo-1 {
          overflow: hidden;
          display: -webkit-box;
          -webkit-line-clamp: 3;
          -webkit-box-orient: vertical;
        }
    </style>
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="horizontal-layout horizontal-menu content-detached-left-sidebar ecommerce-application navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="content-detached-left-sidebar">

    <!-- BEGIN: Header-->
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            
            <div class="content-detached">
                <div class="content-body">
                    
                    <div class="shop-content-overlay"></div>
                    <!-- background Overlay when sidebar is shown  ends-->
                    <!-- Ecommerce Search Bar Ends -->

                    <!-- Ecommerce Products Starts -->
                    
                    @yield('content')
                    <!-- Ecommerce Products Ends -->             
            

                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    @include('components/footer')
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{URL::asset('assets')}}/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{URL::asset('assets')}}/vendors/js/ui/jquery.sticky.js"></script>
    <script src="{{URL::asset('assets')}}/vendors/js/ui/prism.min.js"></script>
    <script src="{{URL::asset('assets')}}/vendors/js/extensions/wNumb.js"></script>
    <script src="{{URL::asset('assets')}}/vendors/js/extensions/nouislider.min.js"></script>
    <script src="{{URL::asset('assets')}}/vendors/js/forms/select/select2.full.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{URL::asset('assets')}}/js/core/app-menu.js"></script>
    <script src="{{URL::asset('assets')}}/js/core/app.js"></script>
    <script src="{{URL::asset('assets')}}/js/scripts/components.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{URL::asset('assets')}}/js/scripts/pages/app-ecommerce-shop.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
        
        $(".mulai_belajar").click(function() {
            id = $(this).attr('id');
            id_user = $("#id_user").val();
            $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });
            $.ajax({ //line 28
                type: 'POST',
                url : '/pilih_pelajaran',
                dataType: 'json',
                data: { id : id,id_user : id_user },
                success: function(data)
                {   
                    if (data.code == 200) {
                        $("#message").remove();
                        $(".toast-body").append('<label style="color:white;font-size:12px;font-weight:bold;" id="message">'+data.message+'</label>')
                        $('.toast').toast('show');
                        $("#ecommerce-products").css('display','none');
                        $("#iframe").css('display','');
                        $("#tambahan_iframe").append('<iframe width="434" height="250" src="'+data.link+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');

                    } else {
                        alert("maaf ada yang salah!!!");
                    }
                }
            });
            
        });
    });
        
    </script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>