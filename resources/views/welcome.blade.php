@extends('layouts.app')
@section('content')
<!-- BEGIN: Body-->
<!-- Dashboard Analytics Start -->

<section id="dashboard-analytics" style="margin-top: -15%;">
    <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="card bg-analytics text-white" style="background-color: #202354">
                <div class="card-content" style="min-height: 239px">
                    <div class="card-body text-center" style="margin-top: 30px">
                        <div class="avatar avatar-xl bg-primary shadow mt-0">
                            
                        </div>
                        <div class="text-center">
                            <h1 class="mb-2 text-white" style="margin-top: 25px">Selamat Datang {{$name}}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-12">
            <div class="card">
                
                <div class="card-header d-flex flex-column align-items-start pb-0" style="min-height: 170px;">
                    <div class="avatar bg-rgba-primary p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-codepen text-primary font-medium-5"></i>
                        </div>
                    </div>
                    <h4 class="text-bold-700" style="margin-top: 3px;">Kelas yang saya ikuti</h4>
                   
                    
                </div>
                <div class="card-footer">
                   
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-12">
            <div class="card">
                <div class="card-header d-flex flex-column align-items-start pb-0" style="min-height: 170px;">
                    <div class="avatar bg-rgba-warning p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-briefcase text-success font-medium-5"></i>
                        </div>
                    </div>
                    <h4 class="text-bold-700">Pelajaran yang sedang berlangsung</h4>
                    
                </div>
                <div class="card-footer">
                    
                </div> 
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: -5px">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <center>
            @if ($is_mobile == "true")
            <h3 style="color:black;font-weight:bold;">PELATIHAN YANG TERSEDIA</h3>
            <span style="margin-bottom: 20px;">Berikut adalah pilihan pelatihan online yang tersedia pada database kami. Silahkan memilih pelatihan yang sesuai dengan minat Anda.</span>
            @else
            <h1 style="color:black;font-size:35px;font-weight:bold;margin-bottom: -3px">PELATIHAN YANG TERSEDIA</h1>
            <span style="font-size: 15px;">Berikut adalah pilihan pelatihan online yang tersedia pada database kami. Silahkan memilih pelatihan yang sesuai dengan minat Anda.</span>
            <br>
            @endif
            </center>
        </div>
    </div>    
</section>
<br>
<section id="ecommerce-products" class="grid-view">
    
    <div class="card ecommerce-card">
        <div class="card-content">
            <div class="item-img text-center" style="margin-top: 5px">
                <a href="javascript:void(0)">
                    <img src="https://e-brochure.s3-id-jkt-1.kilatstorage.id/" alt="avatar" style="max-width: 100px;max-height: 100px;"></a>
            </div>
            <div class="card-body" style="min-height: 150px;margin-top:-30px">
                <div class="item-wrapper">
                    <div class="item-rating">
                        
                    </div>
                    <div>
                        
                    </div>
                </div>
                <div class="item-name text-center">
                    <a href="javascript:void()" style="font-size: 20px;color: black "></a>
                    <p class="item-company">By <span class="company-name">Google</span></p>
                </div>
                <div class="wrapper-1">
                    <p class="demo-1" style="margin-top: 5px;margin-left: -3.5%">
                        
                    </p>
                </div>
            </div>
            <div class="item-options text-center" style="margin-top: 20px">
                <a href="mulai/{{$token}}">                                  
                <div class="btn btn-info btn-block mulai_belajar" id="" style="bottom: 0;position: absolute;">
                    <span class="add-to-cart">Lihat Pelatihan</span> <i class="feather icon-chevrons-right"></i>
                </div></a>
            </div>
        </div>
    </div>
    
</section>
<!-- Dashboard Analytics end -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<!-- <script src="{{URL::asset('assets')}}/vendors/js/vendors.min.js"></script> -->
<script type="text/javascript">
   
    $(function() {
        
    });
    $(document).off('click','#request').on('click','#request', function (){
        //alert("horey"); 
        id = $("#session_id").val();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        //alert("nyampek sini");
        $.ajax({ //line 28
            type: 'POST',
            url : '/request_kelas',
            dataType: 'json',
            data: { id:id },
            success: function(data)
            {
                if (data.code == 200) {
                swal("Permintaan Menjadi Peserta Kelas Terkirim", "Selamat", "success");
                } else {
                swal("Permintaan Menjadi Peserta Kelas Gagal", "Mohon Maaf", "error");   
                }
            }
        });
    });
    // pelajaran
    $(document).off('click','.pelajaran').on('click','.pelajaran', function (){
        id = $(this).attr('id');
        $.ajax({
            url :'/lihat_pelajaran/'+id,
            type: "GET",
            success: function (response) {
                console.log(response);
                if(response) {
                    $(".modal_edit_profile").html(response);
                }
                
            }
        });
    });
                
</script>
@endsection
